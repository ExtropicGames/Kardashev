#ifndef KARDASHEV_BUTTON_H
#define KARDASHEV_BUTTON_H

#include <queue>

#include <iostream>

#include "Widget.h"
#include "Resources.h"

class KS_Button : public KS_Widget {
public:
    KS_Button() {
        // TODO: make sure super is called
        state = 0;
        button_image = NULL;
        clicked_image = NULL;
    }
    
    ~KS_Button() {
        Resources::instance().freeImage(button_image_filename);
        Resources::instance().freeImage(clicked_image_filename);
    }
    
    void setButtonImage(const std::string &filename)  {
        Resources::instance().freeImage(button_image_filename);
        button_image_filename = filename;
        button_image = Resources::instance().loadImage(filename); 
    }
    void setClickedImage(const std::string &filename) {
        Resources::instance().freeImage(clicked_image_filename);
        clicked_image_filename = filename;
        clicked_image = Resources::instance().loadImage(filename); 
    }
    
    void setOnClick(std::function<void(void*)> function, void* args) {
        on_click = function;
        on_click_args = args;
    }
    
    void render(SDL_Surface* screen) {

        // TODO: this code may need some rework in the future
        assert(w() != 0 && h() != 0);
        Sint16 x = -absolute_x();
        Sint16 y = -absolute_y();
        SDL_Rect where = { x, y, (Uint16) (w() - x), (Uint16) (h() - y) };
        
        assert(button_image);
        assert(clicked_image);
        
        if (state == 0) {
            SDL_BlitSurface(button_image, &where, screen, NULL);
        } else {
            int x, y;
            SDL_GetMouseState(&x, &y);
            if (this->contains(x, y)) {
                SDL_BlitSurface(clicked_image, &where, screen, NULL);
            } else {
                SDL_BlitSurface(button_image, &where, screen, NULL);
            }
        }
        
        // TODO: draw the text
        
        super::render(screen);
    }
    
    void update() {
        super::update();
    }
    
    bool onEvent(SDL_Event &event) {
        if (super::onEvent(event)) {
            return true;
        }
        
        if (event.button.button != SDL_BUTTON_LEFT) {
            return false;
        }
        
        if (state == 0) {
            if (event.type == SDL_MOUSEBUTTONDOWN) {
                if (this->contains(event.button.x, event.button.y)) {
                    state = 1;
                    return true;
                }
            }
        } else {
            if (event.type == SDL_MOUSEBUTTONUP) {
                if (this->contains(event.button.x, event.button.y)) {
                    assert(on_click);
                    assert(on_click_args);
                    on_click(on_click_args);
                }
                
                state = 0;
                return true;
            }
        }
        
        return false;
    }
    
private:
    typedef KS_Widget super;
    
    int state;
    
    std::function<void(void*)> on_click;
    void* on_click_args;
    
    std::string button_image_filename;
    SDL_Surface* button_image;
    
    std::string clicked_image_filename;
    SDL_Surface* clicked_image;
    std::string text;
};

#endif
