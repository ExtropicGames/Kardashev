#ifndef KARDASHEV_WINDOW_H
#define KARDASHEV_WINDOW_H

#include <cassert>

#include "Widget.h"
#include "Resources.h"

class KS_Window : public KS_Widget {
public:
    KS_Window() {
        background_image = NULL;
    }
    
    virtual ~KS_Window() {
        Resources::instance().freeImage(background_image_filename);
    }
    
    void setBackgroundImage(const std::string &filename) {
        assert(this != 0);
        Resources::instance().freeImage(background_image_filename);
        background_image = Resources::instance().loadImage(filename);
        background_image_filename = filename;
    }
    
    void render(SDL_Surface* screen) {
        SDL_Rect where = { absolute_x(), absolute_y(), w(), h() };
        
        assert(background_image);
        
        SDL_BlitSurface(background_image, &where, screen, NULL);
        
        super::render(screen);
    }
    
    void update() {
        super::update();
    }
    
    bool onEvent(SDL_Event &event) {
        return super::onEvent(event);
    }
    
private:
    typedef KS_Widget super;
    
    std::string background_image_filename;
    SDL_Surface* background_image;
};

#endif