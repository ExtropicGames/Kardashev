#ifndef RESOURCES_H
#define RESOURCES_H

// C++ includes
#include <map>
#include <utility>
#include <string>

// C includes
#include <cassert>

// Debug includes
#include <iostream>

#include <SDL/SDL_image.h>

// A simple resource manager that relies on reference counting to
// allocate resources.
class Resources {
public:
    static Resources& instance() {
        // thread safe but non-header friendly version
        std::call_once(Resources::singleton, [](){Resources::_instance.reset(new Resources()); });
        return *Resources::_instance;
    }
    
    ~Resources() {
        _instance = NULL;
    }
    
    // TODO: Font loadFont(std::string filename);
    // TODO: void freeFont(std::string filename);
    
    SDL_Surface* loadImage(const std::string &filename) {
        
        ImageMap::iterator result = images.find(filename);
        
        if (result == images.end()) {
        
            SDL_Surface* unoptimized_image = IMG_Load(filename.c_str());
            // TODO: handle nonexistent files more gracefully
            if (!unoptimized_image) {
                return NULL;
            }
            SDL_Surface* optimized_image =  SDL_DisplayFormat(unoptimized_image);
            SDL_FreeSurface(unoptimized_image);
            if (!optimized_image) {
                return NULL;
            }
            
            images[filename] = std::make_pair(1, optimized_image);
            
            return optimized_image;
        } else {
            assert (result->second.second != NULL);
            assert (result->second.first >= 1);
            
            result->second.first++;
            return result->second.second;
        }
    }
    
    void freeImage(const std::string &filename) {

        // we have to check for _instance == NULL here because we can't
        // guarantee order of shutdown when singletons are involved...
        // so Resources might actually be dead, but this function would
        // still get called... wtf.
        if (filename == "" || _instance == NULL) {
            return;
        }
        
        ImageMap::iterator result = images.find(filename);
        
        assert(result != images.end());
        assert(result->second.first >= 1);
        
        if (result->second.first == 1) {
            SDL_FreeSurface(result->second.second);
            images.erase(result);
        } else {
            result->second.first--;
        }
    }
    
private:
    // singleton bullshit
    Resources() { };
    Resources(Resources const&);
    void operator=(Resources const&);
    static std::shared_ptr<Resources> _instance;
    static std::once_flag singleton;

    typedef std::map<std::string, std::pair<unsigned int, SDL_Surface*>> ImageMap;
    ImageMap images;
};

#endif