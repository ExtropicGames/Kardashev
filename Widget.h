#ifndef KARDASHEV_WIDGET_H
#define KARDASHEV_WIDGET_H

#include <SDL/SDL.h>

#include <memory>
#include <vector>

#include <iostream>

//! KS_Widget is the base class that all other Kardashev classes inherit from.
//! KS_Widgets can contain other widgets and have location, width, and height properties.
class KS_Widget {
public:
    KS_Widget() {
        _parent = NULL;
        dimensions = {0, 0, 0, 0};
    }
    
    virtual ~KS_Widget() {
        for (std::shared_ptr<KS_Widget> child : _children) {
            child->_parent = NULL;
        }
        _children.clear();
    }
    
    Sint16 absolute_x() {
        Sint16 x = dimensions.x;
        KS_Widget* p = _parent;
        while (p != NULL) { 
            x += p->x();
            p = p->_parent;
        }
        return x;
    }
    
    Sint16 absolute_y() {
        Sint16 y = dimensions.y;
        KS_Widget* p = _parent;
        while (p != NULL) { 
            y += p->y();
            p = p->_parent;
        }
        return y;
    }

    // Returns position in relative coordinates
    Sint16 x() { return dimensions.x; }
    Sint16 y() { return dimensions.y; }
    Uint16 w() { return dimensions.w; }
    Uint16 h() { return dimensions.h; }

    void setDimensions(SDL_Rect &rect) { dimensions = rect; }
    void setDimensions(Sint16 x, Sint16 y, Uint16 w, Uint16 h) { dimensions = {x,y,w,h}; }
    
    void add(std::shared_ptr<KS_Widget> child) {
        _children.push_back(child);
        child->_parent = this;
    }
    
    bool contains(Sint16 x, Sint16 y) {
        Sint16 _x = absolute_x();
        Sint16 _y = absolute_y();
        return (x >= _x && x < _x + dimensions.w && y >= _y && y < _y + dimensions.h);
    }
    
    // **************************************************
    // * Virtual methods to be implemented by children! *
    // **************************************************
    
    //! Returns true if the event was handled, false if not.
    virtual bool onEvent(SDL_Event &event) {
        // call BEFORE you handle the event
        for (std::shared_ptr<KS_Widget> child : _children) {
            if (child->onEvent(event)) {
                return true;
            }
        }
        
        return false;
    }
    
    virtual void render(SDL_Surface* screen) {
        // call AFTER you render
        for (std::shared_ptr<KS_Widget> child : _children) {
            child->render(screen);
        }
    }
    virtual void update() {
        // call BEFORE you update
        for (std::shared_ptr<KS_Widget> child : _children) {
            child->update();
        }
    }
    
protected:
    SDL_Rect dimensions;
    
    std::vector<std::shared_ptr<KS_Widget>> _children;
private:
    KS_Widget* _parent;
};

#endif